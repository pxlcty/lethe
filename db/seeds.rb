User.create!( name: "Example User",
email: "admin@example.com",
password: "password",
password_confirmation: "password",
user_type: "regular",
admin: true,
shared: false,
activated: true,
activated_at: Time.zone.now )

User.create!( name: "Pelle Svenson",
email: "example@example.com",
password: "password",
password_confirmation: "password",
user_type: "regular",
admin: false,
shared: false,
activated: true,
activated_at: Time.zone.now )

99.times do |n|
	name = Faker::Name.name
	email = "example-#{n+1}@example.com"
	password = "password"
	User.create!(
		name: name,
		email: email,
		password: password,
		password_confirmation: password,
		user_type: "regular",
		admin: false,
		shared: true,
		activated: true,
		activated_at: Time.zone.now )
end
