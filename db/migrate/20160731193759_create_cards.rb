class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :front
      t.text :back
      t.string :hint
      t.integer :level

      t.timestamps null: false
    end
    add_reference :cards, :deck, index: true
    add_foreign_key :cards, :decks
  end
end
