class CreateRosters < ActiveRecord::Migration
  def change
    create_table :rosters do |t|

      t.references  :teacher,     index: true
      t.integer     :student_id
      t.text        :notes

      t.timestamps null: false
    end
    add_foreign_key :rosters, :users, column: :teacher_id
  end
end
