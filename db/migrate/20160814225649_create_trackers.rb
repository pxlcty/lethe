class CreateTrackers < ActiveRecord::Migration
  def change
    create_table  :trackers do |t|

      t.string      :exposure_arr,        array: true, default: []
      t.string      :hint_used_arr,       array: true, default: []
      t.string      :back_exposure_arr,   array: true, default: []
      t.string      :level_changed,       array: true, default: []
      t.string      :front_changed,       array: true, default: []
      t.string      :back_changed,        array: true, default: []
      t.string      :hint_changed,        array: true, default: []

      t.timestamps null: false
    end
    add_reference :trackers, :card, index: true, foreign_key: true
    # I just used these below as examples.
    #add_reference :cards, :deck, index: true
    #add_foreign_key :cards, :decks

  end
end
