class AddTrackedToDeck < ActiveRecord::Migration
  def change
    add_column :decks, :tracked, :boolean, default: false
  end
end
