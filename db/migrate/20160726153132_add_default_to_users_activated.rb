class AddDefaultToUsersActivated < ActiveRecord::Migration
  # def change
  #   change_column_default :users, :activated, true
  # change_column_default :users, :activated, from: nil, to: false
  # end
  def up
    change_column_default :users, :activated, false
  end
  def down
    change_column_default :users, :activated, nil
  end

end
