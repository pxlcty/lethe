class AddPublicTousers < ActiveRecord::Migration
  def change
    add_column :users, :shared, :boolean, default: true
  end
end
