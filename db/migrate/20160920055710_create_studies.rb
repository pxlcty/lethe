class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|

      t.references  :course, index: true
      t.integer     :deck_id
      t.datetime    :release

      t.timestamps null: false
    end
    add_foreign_key :studies, :courses, column: :course_id
  end
end
