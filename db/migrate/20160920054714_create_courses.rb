class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|

      t.references  :teacher,       index: true
      t.datetime    :start_date
      t.datetime    :end_date
      t.string      :title
      t.text        :description

      t.timestamps null: false
    end
    add_foreign_key :courses, :users, column: :teacher_id
  end
end
