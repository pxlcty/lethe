class AddCourseIdToStudy < ActiveRecord::Migration
  def change
    add_column :decks, :course_id, :integer
    add_column :studies, :sent, :boolean, default: false
  end
end
