class CreatePupils < ActiveRecord::Migration
  def change
    create_table :pupils do |t|

      t.references  :course, index: true
      t.integer     :user_id

      t.timestamps null: false
    end
    add_foreign_key :pupils, :courses, column: :course_id
  end
end
