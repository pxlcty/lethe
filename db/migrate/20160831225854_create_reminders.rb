class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|

      t.datetime  :next_alert
      t.string    :alerts,      array: true, default: [] # this is where all future alerts are stored.

      t.timestamps null: false
    end
    add_reference :reminders, :deck, index: true, foreign_key: true
  end
end
