class AddRepeatingToReminders < ActiveRecord::Migration
  def change
    add_column :reminders, :repeating, :boolean
    add_column :reminders, :interval, :integer
  end
end
