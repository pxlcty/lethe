class CreateTeacherInvites < ActiveRecord::Migration
  def change
    create_table :teacher_invites do |t|

      t.references    :user,                  index: true
      t.string        :email
      t.integer       :teacher_id
      t.string        :invitation_scrambled
      t.boolean       :invitation_accepted,    default: false
      t.datetime      :accepted_at

      t.timestamps null: false
    end
    add_foreign_key :teacher_invites, :users, column: :user_id
  end
end
