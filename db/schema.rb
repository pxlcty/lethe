# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160923153235) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cards", force: :cascade do |t|
    t.string   "front"
    t.text     "back"
    t.string   "hint"
    t.integer  "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "deck_id"
    t.string   "picture"
  end

  add_index "cards", ["deck_id"], name: "index_cards_on_deck_id", using: :btree

  create_table "courses", force: :cascade do |t|
    t.integer  "teacher_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "courses", ["teacher_id"], name: "index_courses_on_teacher_id", using: :btree

  create_table "decks", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
    t.boolean  "tracked",     default: false
    t.integer  "course_id"
  end

  add_index "decks", ["user_id"], name: "index_decks_on_user_id", using: :btree

  create_table "pupils", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "pupils", ["course_id"], name: "index_pupils_on_course_id", using: :btree

  create_table "reminders", force: :cascade do |t|
    t.datetime "next_alert"
    t.string   "alerts",     default: [],              array: true
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "deck_id"
    t.boolean  "repeating"
    t.integer  "interval"
  end

  add_index "reminders", ["deck_id"], name: "index_reminders_on_deck_id", using: :btree

  create_table "rosters", force: :cascade do |t|
    t.integer  "teacher_id"
    t.integer  "student_id"
    t.text     "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "rosters", ["teacher_id"], name: "index_rosters_on_teacher_id", using: :btree

  create_table "studies", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "deck_id"
    t.datetime "release"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "sent",       default: false
  end

  add_index "studies", ["course_id"], name: "index_studies_on_course_id", using: :btree

  create_table "teacher_invites", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "email"
    t.integer  "teacher_id"
    t.string   "invitation_scrambled"
    t.boolean  "invitation_accepted",  default: false
    t.datetime "accepted_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "teacher_invites", ["user_id"], name: "index_teacher_invites_on_user_id", using: :btree

  create_table "trackers", force: :cascade do |t|
    t.string   "exposure_arr",      default: [],              array: true
    t.string   "hint_used_arr",     default: [],              array: true
    t.string   "back_exposure_arr", default: [],              array: true
    t.string   "level_changed",     default: [],              array: true
    t.string   "front_changed",     default: [],              array: true
    t.string   "back_changed",      default: [],              array: true
    t.string   "hint_changed",      default: [],              array: true
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "card_id"
  end

  add_index "trackers", ["card_id"], name: "index_trackers_on_card_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "user_type"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.boolean  "admin"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "shared",            default: true
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "cards", "decks"
  add_foreign_key "courses", "users", column: "teacher_id"
  add_foreign_key "decks", "users"
  add_foreign_key "pupils", "courses"
  add_foreign_key "reminders", "decks"
  add_foreign_key "rosters", "users", column: "teacher_id"
  add_foreign_key "studies", "courses"
  add_foreign_key "teacher_invites", "users"
  add_foreign_key "trackers", "cards"
end
