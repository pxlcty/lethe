Rails.application.routes.draw do

  # get 'password_resets/new'
  # get 'password_resets/edit'
  # get 'sessions/new'
  # get 'users/new'

  root 'static_pages#home'

  get     'help'        => 'static_pages#help'
  get     'about'       => 'static_pages#about'
  get     'contact'     => 'static_pages#contact'

  get     'signup'      => 'users#new'
  get     'login'       => 'sessions#new'
  post    'login'       => 'sessions#create'
  delete  'logout'      => 'sessions#destroy'

  resources :users do
    member do
      get 'teacher_tool'
    end
  end
    resources :account_activations,   only: [:edit]
    resources :password_resets,       only: [:new, :create, :edit, :update]


    resources :rosters, only: [:create, :show, :update, :destroy]
    resources :courses do
      resources :pupils
      resources :studies
    end
      resources :teacher_invites, only: [:edit]


  resources :decks do
    member do
      get 'dynamic_list'
      get 'static_list'
      get 'learn_analaysis'
      get 'print'
      get 'repitition_alerts'
      get 'print_custom'
    end

    resources :cards do
      member do
        patch 'skill_update'
        get 'show_next'
        get 'show_previous'
        get 'static_show'
      end
      resources :trackers do
        member do
          get 'see_hint' # need to change to post?
          get 'see_backside'
        end
      end
    end
    # gut feeling tells me that :prints need to be 'after' cards, as they will be used.
    resources :prints, only: [:new, :create, :fake]
    resources :reminders
    #post 'custom/decks/print' => 'print#custom'
  end







  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
