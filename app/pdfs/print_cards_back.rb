require "prawn"
require "prawn/measurement_extensions"

class PrintCardsBack < Prawn::Document

	def initialize(paper_size, card_data)
		@margin = 18
		@body_copy_size = 12
		@paper_size = paper_size
		@height = paper_size[0].to_i
		@width = paper_size[1].to_i
		@card_data = card_data
		#@front_first_y = (@width / 4) * 3 - @margin + (@front_size/2)
		#@front_second_y = (@width / 4) * 1 - @margin + (@front_size/2)

		@first_page = true
		super(page_size: @paper_size, margin: [@margin, @margin, @margin, @margin] ) # margin 1/4 inch
		create_pages
	end

	def create_pages
		@card_data.each do |page|
			if @first_page
				@first_page = false
			else
				start_new_page
			end
			#stroke_axis

			bounding_box([0, 396], width: 252, height: 180, overflow: :truncate) do
				text_box "#{page[0][:back]}",
				at: [0, 180], width: 252, height: 180, overflow: :truncate, valign: :center, align: :center, size: @body_copy_size
			#	transparent(0.5) { stroke_bounds }
			end


			move_cursor_to 216 - @margin
			#stroke_horizontal_rule

			if page.length == 2
				# 2nd card:
				bounding_box([0, 180], width: 252, height: 180, overflow: :truncate) do
					text_box "#{page[1][:back]}",
					at: [0, 180], width: 252, height: 180, overflow: :truncate, valign: :center, align: :center, size: @body_copy_size
			#		transparent(0.5) { stroke_bounds }
				end
			end
		end
	end

end
