require "prawn"
require "prawn/measurement_extensions"

class PrintCardsFront < Prawn::Document

	def initialize(paper_size, card_data)

		puts "------------------------------------------- PRAWN DOCUMENT HERE -------------------------------------------"
		@margin = 18
		@front_size = 18
		@paper_size = paper_size
		@height = paper_size[0].to_i
		@width = paper_size[1].to_i
		@card_data = card_data
		@front_first_y = (@width / 4) * 3 - @margin + (@front_size/2)
		@front_second_y = (@width / 4) * 1 - @margin + (@front_size/2)

		@first_page = true
		super(page_size: @paper_size, margin: [@margin, @margin, @margin, @margin] ) # margin 1/4 inch
		# text "FRONT PAGE"
		# text "documend should be #{@width} wide and #{@height} tall."
		# text "There should be #{@cards_per_page} on each page."
		# text "#{paper_size} is #{@width.send(:in)} and #{@height.send(:in)}"
		# text "current size is : "
		# text "full content is:"
		# @card_data.each do |entry|
		# 	text "#{entry[0]}"
		# 	text "---------------"
		# 	text "#{entry[1]}"
		# 	text "---------------"
		# end
		create_pages
	end

	def create_pages
		@card_data.each do |page|
			if @first_page
				@first_page = false
			else
				start_new_page
			end
#			stroke_axis


			text "#{page[0][:hint]}", color: 'CCCCCC', style: :italic, size: 8, align: :right

			move_cursor_to @front_first_y
			text "#{page[0][:front]}", align: :center, size: @front_size

			move_cursor_to 224
			text "#{page[0][:deck]}", size: 8

			move_cursor_to 216 - @margin
			stroke_horizontal_rule

			if page.length == 2
				# 2nd card:
				move_down @margin
				text "#{page[1][:hint]}", color: 'CCCCCC', style: :italic, size: 8, align: :right

				move_cursor_to @front_second_y
				text "#{page[1][:front]}", align: :center, size: @front_size

				move_cursor_to 8
				text "#{page[1][:deck]}", size: 8
			end
		end
	end

end
