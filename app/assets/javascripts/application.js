// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require main
//= require cards
//= require prints
//= require bootstrap-datepicker

//= require_tree .

var cards_for_printing = [];
function add_card_to_print_list(card) {
	console.log("input value was: ", card);
	var href = $("#custom_cards").attr('href');
	var replacement_beginning = href.indexOf("custom_select_items=");
	var replacement_end = href.indexOf("&paper_height");
	console.log("start is index: ", replacement_beginning);
	console.log("end is index: ", replacement_end);
	var href_end = href.substring(replacement_end, href.length);
	var href_start = href.substring(0, replacement_beginning);

	console.log("href start is ; ", href_start);
	console.log("href end is ; ", href_end);

	$("#custom_cards").attr('href', function(){
		console.log("href is : ", href);
		var pattern = /custom_select_items=((\d*,?)*)&/ig;
		//var str = "The rain in SPAIN stays mainly in the plain";
		//console.log(str);
		//var res = str.match(/ain/g);

		var search = href.match(pattern);
		console.log("regex returns : ", search);
		cards_list_string = search[0];
		console.log("cards_list is a ", cards_list_string);
		id_collection_start_at = cards_list_string.indexOf('=');
		id_collection = cards_list_string.substring((id_collection_start_at +1), (cards_list_string.length -1));
		console.log("we should have an empty string here: ", id_collection);

		console.log("does this still exist?", card);
		// .map(Number) forces string numbers to be NUMBERS:
		id_collection = id_collection.split(',').map(Number);
		if(id_collection.indexOf(card) == -1) {
			console.log('- - - - - - - - -  NEWS FLASH - - - - - - - - -\n', card, " DOES NOT exist");
			id_collection.push(card);
		} else {
			console.log('- - - - - - - - -  NEWS FLASH - - - - - - - - -\n', card, " pre exists");
		//if(id_collection.includes(card)) {
			var index = id_collection.indexOf(card);
			id_collection.splice( index, 1 );
		}
		console.log("before toString", id_collection);
		if (id_collection[0] == "") {
			id_collection.shift();
		}
		new_url = id_collection.toString();
		console.log("return is : ", new_url);
		new_url =	href_start + "custom_select_items=" + new_url + href_end;
		return new_url;

	});
	console.log("\n= = = = = = \nstring is now : ", $("#custom_cards").attr('href'));

	// var url_start = href.indexOf("custom_select_items=");
	// var url_end = href.indexOf("&paper_height");
	// console.log("read url string: ", href);
	// console.log("custom_select_items=, starts at ", url_start);
	// console.log("&paper_height, start at ", url_end);


// this was the original script  :
	// if (cards_for_printing.includes(card)) {
	//
	// 	var index = cards_for_printing.indexOf(card);
	// 	if(index != -1) { cards_for_printing.splice( index, 1 ); }
	// } else {
	// 		cards_for_printing.push(card);
	// }
	//console.log("array is now: ", cards_for_printing.length, " long."	);
	//return "I am returning this excuse as to not cause confusion";
}
