class TeacherInvitesController < ApplicationController

	def edit
		invited_user = User.find_by(email: params[:email])
		puts "did we get correct email: #{invited_user.email}"
		@invitation = TeacherInvite.find_by(user_id: invited_user)
		puts "Do we have an invite? :#{@invitation.email}"
		# ---matching token vs digest: ------
		puts "this is the BIG one:::: "
		if authenticate_invite(params[:id])# this is the token value
			@invitation.invitation_accepted = true
			@invitation.accepted_at = DateTime.now
				if @invitation.save
					add_to_roster = Roster.create(
						teacher_id: @invitation.teacher_id,
						student_id: current_user.id
					)
					teacher = User.find(@invitation.teacher_id)
					UserMailer.invitation_accepted(teacher, current_user).deliver_now
				redirect_to user_path(current_user)
			end
		end
	end

	def authenticate_invite(token)
		puts "authentication started..."
		digest = @invitation.send('invitation_scrambled')
		puts 'digest sent...'
		return false if digest.nil? # if multiple browser used, one log's out, force all browsers to logout.
		BCrypt::Password.new(digest).is_password?(token)
	end

	#user = User.find_by(email: params[:email])

end
