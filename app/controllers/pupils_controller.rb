class PupilsController < ApplicationController

	def create
		puts "Did we get anything? #{params[:pupil]}"
		@pupil = Pupil.new(pupil_params)
		puts "#{@pupil.id}"
		puts @pupil.course_id
		puts @pupil.user_id
		@course = Course.find(params[:course_id])
		if @pupil.save
			flash[:info] = "Pupil added."
			redirect_to course_path(@course)
		end
	end

	def destroy
		puts "we sure did destroy"
		Pupil.find(params[:id]).destroy
		flash[:success] = "Pupil removed from course."
		@course = Course.find_by(id: params[:course_id])
		redirect_to course_path(@course)

	end


	private
		def pupil_params
			params.require(:pupil).permit(:course_id, :user_id)
		end
end
