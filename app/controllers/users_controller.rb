class UsersController < ApplicationController

  before_action :logged_in_user, only: [:index, :show, :edit, :update]
  before_action :correct_user, only: [:edit, :update]
  #before_action :delete_access, only: :destroy # this needs to work!!!
  #before_action :admin_user, only: :destroy

  def index
  #  @users = User.all
    if current_user.admin
      @users = User.paginate(page: params[:page], per_page: 50)
    else
      @users = User.where(shared: true).where(activated: true).paginate(page: params[:page], per_page: 30)
    end
  end

  def show
    @user = User.find(params[:id])
    @decks = Deck.where(user_id: @user.id)
    @deck = Deck.new
    if current_user?(@user) || current_user.admin? || @user.shared? # check if user is 'current user', admin or is publicly shared?
    else
       flash[:danger] = 'Not publicly accessible user. User must set public to true in Account Settings.'
       redirect_to(root_url)
     end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated."
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User account deleted."
    redirect_to users_url
  end

  def teacher_tool
    @teacher = User.find(params[:id])
    unless current_user.user_type == 'teacher'
      redirect_to @teacher
    end
    @rosters = Roster.where(teacher_id: @teacher.id)
    @courses = Course.where(teacher_id: @teacher.id)
    @decks = Deck.where(user_id: @teacher.id)
    @roster = Roster.new
    @course = Course.new
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :shared)
    end

    # Before filters

    # Confirm a logged-in user. THIS HAS BEEN MOVED TO APPLICATIONCONTROLLER
    # def logged_in_user
    #   unless logged_in?
    #     store_location
    #     flash[:danger] = "Please log in."
    #     redirect_to login_url
    #   end
    # end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

    def delete_access
      unless correct_user || current_user.admin?
        flash[:danger] = "You're not authorized to delete this account."
        reidrect_to(root_url)
      end
    end

end
