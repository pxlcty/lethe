class StudiesController < ApplicationController

	def create
		@study = Study.new(study_params)
		@study.release = Date.strptime(params[:study][:release] , "%m/%d/%Y").strftime("%Y/%m/%d %H:%M:%S")
		@study.course_id = params[:course_id]
		puts "did we get deck id?: #{@study.deck_id}"
		@course = Course.find(params[:course_id])
		if @study.save
			flash[:info] = "Deck added."
			redirect_to course_path(@course)
		end
	end

	def destroy
		Study.find(params[:id]).destroy
		flash[:success] = "Study was removed from course."
		@course = Course.find_by(id: params[:course_id])
		redirect_to course_path(@course)
	end

	private

		def study_params
			params.require(:study).permit(:deck_id, :release, :course_id)
		end
end
