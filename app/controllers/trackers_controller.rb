class TrackersController < ApplicationController

	def show
		@tracker = Tracker.find(params[:id])
	end

	def see_hint
		#deck = Deck.find(params[:deck_id])
		#card = Card.find(params[:card_id])
		tracker = Tracker.find(params[:id])
		hintTime = tracker.hint_used_arr.push(Time.zone.now)
		tracker.update_attribute(:hint_used_arr, hintTime)
		respond_to do |format|
			format.js
		end
	end

	def change_hint
	end

	def see_backside
		tracker = Tracker.find(params[:id])
		hintTime = tracker.hint_used_arr.push(Time.zone.now)
		tracker.update_attribute(:hint_used_arr, hintTime)
		respond_to do |format|
			format.js
		end
	end

	def change_backside
	end

	def change_skill_level
	end

end
