class RemindersController < ApplicationController

	def show
#		if Reminder.exists?(id: params[:id])
			@reminder = Reminder.find(params[:id])
		# else
		# 	flash[:info] = "Looks like all your alerts are passed."
		# 	redirect_to repitition_alerts_deck_path(params[:id))
		# end

	end

	def new
		@deck = Deck.find(params[:id])
		redirect_to repitition_alerts_deck_path(@deck)
	end

	def create
		logger.info# request.env
		puts "create was called"
		puts "interval is : #{params[:reminder][:interval]}"
		puts "next_alert is : #{params[:reminder][:next_alert]}"
		puts "alerts is : #{params[:reminder][:alerts]}"
		puts "did we get the deck by any chance? #{params[:deck_id]}"
		@reminder = Reminder.new(reminder_params)
		@reminder.deck_id = params[:deck_id]
		if params[:commit] == "Create Daily alerts"
			interval = params[:reminder][:interval].to_i
			day_count = params[:reminder][:day_count].to_i

				@reminder.next_alert = (DateTime.now + interval.days)
				# REVISE :: FIGURE OUT A BETTER WAY FOR THIS::::
				alerts_arr = Array.new(day_count + 2){ |index| DateTime.now + ( (index * interval)  ).days  }
				# REMOV BELOW LINE TO SEE PROBLEM:
				alerts_arr.shift(2)
				alerts_arr.pop
				@reminder.alerts = alerts_arr
		elsif params[:commit] == "Create Weekly alerts"
			interval = params[:reminder][:interval].to_i
			week_count = params[:reminder][:week_count].to_i

			@reminder.next_alert = (DateTime.now + interval.weeks)

			# REVISE :: FIGURE OUT A BETTER WAY FOR THIS::::
			alerts_arr = Array.new(week_count + 2){ |index| DateTime.now + ( (index * interval) * 7 ).days  }
			# REMOV BELOW LINE TO SEE PROBLEM:
			alerts_arr.shift(2)
			alerts_arr.pop
			@reminder.alerts = alerts_arr
		else
			@reminder.alerts = params[:reminder][:alerts]
			@reminder.next_alert = params[:reminder][:next_alert]
		end


		if @reminder.save
			flash[:success] = "Reminder was saved"
			render 'show'
		else
			flash[:danger] = "Reminder was NOT saved"
			render 'new'
		end
	end

	private

		def reminder_params
			params[:reminder][:alerts] ||= [] # this is if the array is empty.
			# putting the array inside a hash.
			params.require(:reminder).permit(:deck_id, :interval, :next_alert, {alerts: []}, :repeating)
		end

end
