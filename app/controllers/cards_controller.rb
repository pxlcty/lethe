class CardsController < ApplicationController

	def show
		#1st you retrieve the post thanks to params[:post_id]
		deck = Deck.find(params[:deck_id])
		#2nd you retrieve the comment thanks to params[:id]
		@card = deck.cards.find(params[:id])
#		@tracker = Tracker.new --This should be created already. Test and cofirm to remove.
		@current_collection_arr = Card.where(deck_id: deck) # who needs this? Test and cofirm to remove.
	end

	def create
		deck = Deck.find(params[:deck_id])
		@card = deck.cards.create(card_params)
		if @card.save
			if deck.tracked
				Tracker.create!(card_id: @card.id)
			end
			flash[:success] = "New card was created."
			redirect_to deck_path(deck)
		else
			flash[:danger] = "Deck was not created"
			redirect_to deck_path(@card.deck_id)
		end
	end

	def edit
		@card = Card.find(params[:id])
	end

	def update
		@card = Card.find(params[:id])
		if @card.deck.tracked
			detect_changes
		end
		if @card.update_attributes(card_params)
			if @card.deck.tracked
				prop_changed?
			end
			flash[:success] = "Card info updated."
			respond_to do |format|
				format.html { render 'show' }
			end
		else
			render 'edit'
		end
	end

	def destroy
		deck = Deck.find(params[:deck_id])
		@card = Card.find(params[:id]).destroy
		flash[:success] = "Card has been deleted."
		redirect_to deck_path(deck)
		#redirect_to current_user
	end

	def skill_update # remove to tracker_cotroller.rb? Test and cofirm to remove.
		@card = Card.find(params[:id])
		if @card.update_attributes(card_params)
			tracker = Tracker.find_by(card_id: @card.id)
			levelChange = tracker.level_changed.push([Time.zone.now, @card.level])
			tracker.update_attribute(:level_changed, levelChange)
			# respond_to do |format|
			# 	format.js { render 'shared/no_visual_feedback'}
			# end
			respond_to do |format|
				format.js
			end
		end
#		flash[:danger] = "Update of skill level didn't save."
#		render 'show'
	end

	def show_next
		list_arr = cookies.permanent[:cards_list].split(',')
		list_arr.rotate!(1)
		cookies.permanent[:cards_list] = (list_arr.class == Array) ? list_arr.join(',') : ''
		deck = Deck.find(params[:deck_id])
		redirect_to deck_card_path(deck, list_arr[0])
	end

	def show_previous
		list_arr = cookies.permanent[:cards_list].split(',')
		list_arr.rotate!(-1)
		cookies.permanent[:cards_list] = (list_arr.class == Array) ? list_arr.join(',') : ''
		deck = Deck.find(params[:deck_id])
		redirect_to deck_card_path(deck, list_arr[0])
	end

	def static_show
		# this is only used if clicked on from deck/show page w/out choosing a static or random list option.
		@deck = Deck.find(params[:deck_id])
		list_arr = Card.where(deck_id: @deck.id).ids
		this_card = Card.find(params[:id]).id
		position = list_arr.index(this_card)
		list_arr.rotate!(position)
		# this exists in deck controller. Maybe move to application_controller?
		cookies.permanent[:cards_list] = (list_arr.class == Array) ? list_arr.join(',') : '' # pushes user.id into user_id; encrypts user_id, adds 20 years to it, then bakes a cookie out of it.
		redirect_to deck_card_path(@deck, list_arr[0])
	end

	private

	def card_params
		params.require(:card).permit(:front, :hint, :level, :back, :deck_id, :picture)
	end

	def detect_changes
		@changed = []
		@changed << :front if @card.front != params[:card][:front]
		@changed << :hint if @card.hint != params[:card][:hint]
		@changed << :back if @card.back != params[:card][:back]
	end

	def prop_changed?
		@changed.each do |check|
			@changed.include? check
			puts "Following attribute was changed : #{check}"
			update_tracker(check)
		end
	end

	def update_tracker(card_attribute)
		tracker_attribute =  case card_attribute
		when :front; :front_changed
		when :back; :back_changed
		when :hint; :hint_changed
		end
		string_tracker_column = tracker_attribute.to_s
		@tracker ||= Tracker.find_by(card_id: @card.id)
		update_array = @tracker[tracker_attribute]
		puts "update_array is #{update_array}"
		# if update_array.length == 0 # if update_array is [[]], fix:
		# 	update_array = []
		# 	puts "---------------   update_array was set to []   ---------------"
		# end

		puts "did update_array get correct content? #{update_array}"
		update_array << [Time.zone.now, @card[card_attribute]]
		puts "last info to push into array is #{update_array[-1]}"
		puts "full data array looks like this : #{update_array}"
		@tracker.update_attribute(tracker_attribute, update_array)
		#tracker.update_attribute(:hint_used_arr, hintTime)
	end

end
