class PrintsController < ApplicationController

	def new
		#response.headers["Content-Type"] = "application/pdf"
		puts "------------PPPPPPlease say this is: ", params[:custom_select_items];
		if params[:custom_select_items].blank?
			setup_standard_params('all')

			deck_ref = Deck.find(params[:deck_id])
			if deck_ref.attribute_present?(:course_id)
				course = Course.find_by(id: deck_ref.course_id)
				teacher = User.find(course.teacher_id)
				CourseMailer.student_print(current_user, teacher, deck_ref).deliver
			end
		else
			@curated_list = params[:custom_select_items].split(',')
			setup_standard_params('selected')
			deck_ref = Deck.find(params[:deck_id])
			if deck_ref.attribute_present?(:course_id)
				course = Course.find_by(id: deck_ref.course_id)
				teacher = User.find(course.teacher_id)
				CourseMailer.student_print(current_user, teacher, deck_ref).deliver
			end
		end
		get_respond_block

	end

	def create
		setup_standard_params('selected')
		get_respond_block
	end

	private

		def setup_standard_params(cards_select = 'all')
			@paperSize = [params[:paper_height].to_i, params[:paper_width].to_i]
			@cardCount = params[:cards_per_paper]
			if cards_select == 'all' ? @cards = Card.where(deck_id: params[:deck_id]) : @cards = Card.where(id: (params[:custom_select_items].split(",").map { |s| s.to_i })); end
			convert_to_points(@paperSize, 'inches')
			@card_data = create_input_data(@cards, @cardCount)
		end

		def create_input_data (cards, cards_per_page)
			full_collection = []
			page_collection = []
			cards_info = {}

			cards.each_with_index do |card, index|
				cards_info = { front: card.front, deck: card.deck.name, hint: card.hint, back: card.back }
				page_collection.push(cards_info)
				if page_collection.length.to_i % cards_per_page.to_i == 0
					full_collection.push(page_collection)
					page_collection = []
				end
				if index == cards.size - 1 && page_collection.length.to_i % cards_per_page.to_i != 0
					full_collection.push(page_collection)
				end
			end
			return full_collection
		end

		def convert_to_points(size, measurement)
			conversion = 0
			if measurement == 'inches'
				conversion = 72
			end
			size[0] *= 72
			size[1] *= 72
		end

		def get_respond_block
			respond_to do |format|
				#format.js
				format.pdf do
					if params[:side] == 'front'
						puts "how many items do I have to render: #{@card_data}"
						pdf = PrintCardsFront.new(@paperSize, @card_data)
						send_data pdf.render, filename: "cards_frontside.pdf",
																type: "application/pdf",
																disposition: "inline"
#						redirect_to "deck/#{params[:deck_id]}/prints.new/pdf"
					else
					 pdf = PrintCardsBack.new(@paperSize, @card_data)
					 send_data pdf.render, filename: "cards_backside.pdf",
																type: "application/pdf",
																disposition: "inline"
					end
				end
			end

		end

		def get_respond_block_forced
			headers["Content-Type"] ||= 'application/'
			respond_to do |format|
				#format.js
				format.pdf do
					if params[:side] == 'front'
						puts "how many items do I have to render: #{@card_data}"
						pdf = PrintCardsFront.new(@paperSize, @card_data)
						send_data pdf.render, filename: "cards_frontside.pdf",
																type: "application/pdf",
																disposition: "inline"
						# puts "what is the pdf itself right now?: #{pdf}"
						# puts "what is the pdf itself right now?: #{:pdf}"
						# url = '/decks/'+ location.pathname.match(/^\/decks\/(\d+)/)[1] +'/prints/create.pdf'
						# puts "url is : #{url}"
						# redirect_to url
						#send_file pdf, :type=>"application/pdf"
					else
						pdf = PrintCardsBack.new(@paperSize, @card_data)
						send_data pdf.render, filename: "cards_backside.pdf",
																type: "application/pdf",
																disposition: "inline"
						#redirect_to pdf

					end
				end

			end

		end



end
