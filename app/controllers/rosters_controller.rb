class RostersController < ApplicationController

	# def index
	# 	 @teacher = User.find(current_user.id)
	# 	 @rosters = Roster.where(teacher_id: current_user.id)
	# 	 @roster = Roster.new
	# end

	def show
		@roster = Roster.find(params[:id])
		@student = User.find(@roster.student_id)
		@pupils = Pupil.where(user_id: @student)
		@courses = Course.where(id: @pupils.pluck(:course_id) )
		@teacher_courses = Course.where(teacher_id: current_user.id)

		# should get all decks associated with teachers courses:
		@studies = Deck.where(course_id: @teacher_courses.pluck(:id))


		puts "Teacher has #{@teacher_courses.count}"
		#@rosters_decks = Decks.where(course_id: @teacher_courses.pluck[:id])
	end

	# def main
	# 	@teacher = User.find(current_user.id)
	# 	@rosters = Roster.where(teacher_id: current_user.id)
	# end

	def create
		puts "did we get params: #{params[:roster][:roster_invite_email]}"
		if params[:roster][:name] == ""
			name = params[:roster][:roster_invite_email].to_s.split(/@/).first
		else
			name = params[:roster][:name]
		end

		new_student_email = params[:roster][:roster_invite_email]
		if already_exist?(new_student_email)
			puts "user exists already, send invitation to existing user..."
			send_invitation_to_existing_user(new_student_email)
		else
			puts "creating new user"
			create_new_student_user(name, new_student_email)
		end
	end

	def update
		@roster = Roster.find(params[:id])
		if @roster.update_attributes(roster_params)
			flash.now[:success] = "Notes saved."
		end
	end

	private

		def roster_params
			params.require(:roster).permit(:notes)
		end

		def already_exist?(email)
			User.exists?(email: email)
		end

		def send_invitation_to_existing_user (email)
			puts "="*80, "Send Invite to EXISTING user", "="*80
			invite_user = User.find_by(email: email)
			new_invite = TeacherInvite.create(
				user:				invite_user,
				email:			email,
				teacher_id:	current_user.id
			)
			UserMailer.teacher_invitation(invite_user, current_user, new_invite).deliver_now
			flash.now[:info] = "Email already exists, acceptance of teacher email sent to exissting user."
		end

		def create_new_student_user(name, email)
			puts "session id is now: #{current_user.id}"
			password = ""; 8.times{password << (65 + rand(25)).chr}
			@new_student = User.create(
				name: name,
				email: email,
				shared: false,
				user_type: 'student',
				admin: false,
				password: password,
				password_confirmation: password
			)
			@new_student.send_activation_email
			flash.now[:info] = "Activation email was sent to #{name} at #{email}."
			Roster.create(
				teacher_id: current_user.id,
				student_id: @new_student.id
			)
			puts "session id is now: #{current_user.id}"
		end

end
