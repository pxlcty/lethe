class DecksController < ApplicationController

	def show
		@deck = Deck.find(params[:id])
		@cards = Card.where(deck_id: @deck.id)
		@card = Card.new
		cards_list = @deck.deck_create_static_list
		create_cookie_cards_list(cards_list)
	end

	def create
		@deck = Deck.new(deck_params)
		@deck.user_id = current_user.id
		#byebug
		params[:deck][:tracked] == '1' ? @deck.tracked = true : @deck.tracked = false
		if @deck.save
			flash[:success] = "Deck was created"
			redirect_to user_path(current_user.id)
		else
			flash[:danger] = "Deck was not created"
			redirect_to user_path(current_user.id)
		end
	end

	def edit
		@deck = Deck.find(params[:id])
	end

	def update
		@deck = Deck.find(params[:id])
		if @deck.update_attributes(deck_params)
			flash[:success] = "Deck settings were updated."
			redirect_to deck_path(@deck)
		else
			render 'edit'
		end
	end

	def destroy
		Deck.find(params[:id]).destroy
		flash[:success] = "Deck and all cards belonging to it have been removed."
		redirect_to current_user
	end

	def dynamic_list
		@deck = Deck.find(params[:id])
		cards_list = @deck.create_smart_randomlist
		create_cookie_cards_list(cards_list)
		redirect_to deck_card_path(@deck, cards_list[0])
	end

	def static_list
		@deck = Deck.find(params[:id])
		cards_list = @deck.deck_create_static_list
		create_cookie_cards_list(cards_list)
		redirect_to deck_card_path(@deck, cards_list[0])
	end

	def learn_analaysis
		@deck = Deck.find(params[:id])
		@cards = Card.where(deck_id: @deck.id)
		@trackers = Tracker.where(card_id: @cards.ids)
	end

	def print
		@deck = Deck.find(params[:id])
		@card_ids = Card.where(deck_id: @deck.id).ids
	end

	def print_custom
		@deck = Deck.find(params[:id])
		@cards = Card.where(deck_id: @deck.id)
	end

	def repitition_alerts
		@deck = Deck.find(params[:id])
		if Reminder.exists?(deck_id: @deck.id).present?
			puts "----------------------------- CHECK IF REMINDER ALREADY EXIST? -----------------------------"
			@reminder = Reminder.find_by(deck_id: @deck.id)
			redirect_to deck_reminder_path(@deck, @reminder)
		else
			puts "----------------------------- CREATE NEW REMINDER -----------------------------"
			@reminder = Reminder.new
		end
	end

	private

	def deck_params
		params.require(:deck).permit(:name, :description, :user_id, :tracker)
	end

	def create_cookie_cards_list(list_arr)
		cookies.permanent[:cards_list] = (list_arr.class == Array) ? list_arr.join(',') : '' # pushes user.id into user_id; encrypts user_id, adds 20 years to it, then bakes a cookie out of it.
	end

end
