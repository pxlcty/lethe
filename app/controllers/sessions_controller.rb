class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email or incorrect password'
      render 'new'
    end
  end

  def destroy
    # if case is comes from a scenario with multiple browser windows logged into the same user.
    # all windows will be logged out.
    log_out if logged_in? # log_out will only be triggered if logged_in? is true.
    redirect_to root_url
  end

end
