class CoursesController < ApplicationController

	def show
		@teacher = current_user
		@rosters = Roster.where(teacher_id: current_user.id)
		@course = Course.find(params[:id])
		@pupils = Pupil.where(course_id: @course.id)
		non_enrolled_ids = non_enrolled_students(@pupils, @rosters)
		#@non_enrolled = User.where(id: non_enrolled_ids)
		@rosters_left = Roster.where(student_id: non_enrolled_ids)
		#-----------------------
		@study = Study.new
		@studies = Study.where(course_id: @course.id).order(release: :asc)
#		@studies.order(release: :desc)
		#@decks_not_included
		@teachers_decks = Deck.where(user_id: current_user.id)

#		@pupil = Pupil.new
#		@pupil_ids = @pupils.ids
		#puts "enrolled are : #{@enrolled.count}"

#		@non_enrolled = User.where(id: @rosters)# - @enrolled
#		@non_enrolled.where.not(@enrolled)
##		puts "non enrolled are : #{@non_enrolled.count}"

	#	@teacher_decks = Deck.where(user_id: current_user.id)
	#	puts "pupils id is : #{@pupils.first.user_id}"
	#	@rosters.where.not(student_id: Pupil.where(course_id: @course.id))
#		@limited_rosters = @rosters.where.not(student_id: Pupil.where(course_id: @course.id))

	end

	def new
		@course = Course.new
	end

	def create
		@course = Course.new(course_params)
		@course.teacher_id = current_user.id
		@course.start_date = Date.strptime(params[:course][:start_date] , "%m/%d/%Y").strftime("%Y/%m/%d %H:%M:%S")
		@course.end_date = Date.strptime(params[:course][:end_date] , "%m/%d/%Y").strftime("%Y/%m/%d %H:%M:%S")
		if @course.save
			flash[:success] = "Course initialization was saved. Now add pupils from your roster or studies from your personal decks."
			redirect_to course_path(@course)
		end
	end


	private
		def course_params
			params.require(:course).permit(:title, :description, :start_date, :end_date)
		end

		def non_enrolled_students (pupils, roster)
			#puts "what is : #{pupils.first.user_id}"
			enrolled_ids = pupils.pluck(:user_id)
			roster_ids = roster.pluck(:student_id)
			non_enrolled_from_roster = roster_ids - enrolled_ids
			puts "enrolled count is : #{enrolled_ids}"
			puts "roster count: #{roster_ids}"
			puts "non_enrolled_from_roster is : #{non_enrolled_from_roster}"
			return non_enrolled_from_roster
		end

end
