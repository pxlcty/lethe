class TeacherInvite < ActiveRecord::Base
	belongs_to :user

	attr_accessor :invitation_token # Not allowed:, :invitation_scrambled
	# this does not 'force' the :invitation_scrambled to be included in the DB save!!!

	before_create :create_invitation_digest

	# Returns a random token.
	def TeacherInvite.new_token
		puts "token triggered"
		SecureRandom.urlsafe_base64
	end

	def TeacherInvite.scramble(string)
		puts "Scrambling triggered"
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
																									BCrypt::Engine::cost
		BCrypt::Password.create(string, cost: cost)
	end


	private
		def create_invitation_digest
			self.invitation_token = TeacherInvite.new_token
			self.invitation_scrambled = TeacherInvite.scramble(invitation_token)
		end

end
