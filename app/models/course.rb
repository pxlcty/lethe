class Course < ActiveRecord::Base

	belongs_to 	:user
	has_many		:pupils
	has_many		:studies
	
end
