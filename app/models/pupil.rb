class Pupil < ActiveRecord::Base

	belongs_to 	:course
	has_one			:user # student
	
end
