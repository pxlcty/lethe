class Roster < ActiveRecord::Base

	belongs_to :user #teacher
	has_many	 :pupils

	attr_accessor :roster_invite_email, :name

end
