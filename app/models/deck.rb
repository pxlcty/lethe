class Deck < ActiveRecord::Base

	belongs_to 		:user
	has_many 			:cards, dependent: :destroy
	has_one				:reminder, dependent: :destroy

	validates			:name, presence: true, length: { minimum: 1 }
	validates			:user_id, presence: true

	def create_smart_randomlist
		id_level_array = Card.where(deck_id: self.id).pluck(:id, :level)
		#id_level_array = Card.where(deck_id: @card.deck).pluck(:id, :level)
		@learn_list = [] # clear list if existing.
		expose_card_level_multiplication(id_level_array)
		id_level_array.each do |item|
			@learn_list.fill(item[0], @learn_list.size, item[1])
		end
		@learn_list.shuffle! # figure out this? http://stackoverflow.com/questions/19261061/picking-a-random-option-where-each-option-has-a-different-probability-of-being
		@learn_list = no_double_exposures(@learn_list)
		return @learn_list
	end

	def deck_create_static_list
		@learn_list = Card.where(deck_id: self.id).ids
		return @learn_list
	end

	def next_card_from_list(direction = 0)
		return @learn_list.rotate!(direction)
	end

	# def create_cookie_cards_list(list_arr)
	# 	cookies.permanent[:cards_list] = (list_arr.class == Array) ? list_arr.join(',') : '' # pushes user.id into user_id; encrypts user_id, adds 20 years to it, then bakes a cookie out of it.
	# end

	# def create_pdf
	# end


	private

		# change exposure levels to your needs.
		def expose_card_level_multiplication(nested_array)
				nested_array.each { |id_level|
						id_level[1] = case id_level[1]
							when 1
								12
							when 2
								8
							when 3
								4
							when 4
								2
							when 5
								1
							end
				}
				return nested_array
			end

			def no_double_exposures(shuffled_arr)
				newArr = []
				# make sure first & last are not the same.
				if(shuffled_arr[0] == shuffled_arr[-1])
					shuffled_arr.pop
				end
				previous = nil
				for count in shuffled_arr
					#puts "Fresh count is: #{count} & #{previous}"
					if previous.nil?
					#	puts "nil was true the first time"
						previous = count
						newArr.push(count)
						next
					elsif previous != count
					#	puts "ADDING: #{previous} vs #{count}"
						newArr.push(count)
						previous = count
					else
					#	puts "NOT adding: #{previous} vs #{count}"
					end
				#puts "previous changed to #{previous}"
				#puts ""
				end
				return newArr
			end


end
