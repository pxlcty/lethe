class User < ActiveRecord::Base

	has_many 				:decks
	has_many				:teacher_invites

	attr_accessor 	:remember_token, :activation_token, :reset_token

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

	before_save 		:downcase_email
	before_create 	:create_activation_digest

	validates 			:name, presence: true, length: { maximum: 50 }
	validates				:email, presence: true, length: { maximum: 255 },
													format: { with: VALID_EMAIL_REGEX },
													uniqueness: { case_sensitive: false }

	has_secure_password
	validates :password, length: { minimum: 6 }, allow_blank: true # this allows for updates to require password change.

	#Returns the hash digest of the given string.
	# This is for fixtures to use so we can 'see' the password and pass it in 'unhashed' to get it back hashed.
	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
																									BCrypt::Engine::cost
		BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random token.
	def User.new_token
		SecureRandom.urlsafe_base64
	end

	# Remember a user in the DB for use in persistent sessions.
	def remember
		self.remember_token = User.new_token #remember_token is set by attr_accessor!
		update_attribute(:remember_digest, User.digest(remember_token)) # this digests the remember token, then add's it to the DB column remmber_digest
	end

	# Forget a user. (We try to break cookies functionality here)
	def forget
		update_attribute(:remember_digest, nil)
	end

	# Returns true if given token matches the digest
	def authenticated?(attribute, token)
		digest = send("#{attribute}_digest")
		return false if digest.nil? # if multiple browser used, one log's out, force all browsers to logout.
		BCrypt::Password.new(digest).is_password?(token)
	end

	def activate
		update_columns(activated: true, activated_at: Time.zone.now )
		#update_attribute(:activated, true)
		#update_attribute(:activated_at, Time.zone.now)
	end

	def send_activation_email
		UserMailer.account_activation(self).deliver_now
	end

	# Sets the apssword reset attributes
	def create_reset_digest
		self.reset_token = User.new_token
		update_columns( reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now )
#		update_attribute(:reset_digest, User.digest(reset_token) )
#		update_attribute(:reset_sent_at, Time.zone.now )
	end

	# Sends password reset email
	def send_password_reset_email
		UserMailer.password_reset(self).deliver_now
	end

	# Returns true if a password reset has expired.
	def password_reset_expired?
		reset_sent_at < 2.hours.ago
	end

	private

		# Converts email to all lower-case
		def downcase_email
			self.email = email.downcase
		end

		# Create and assigns the activation token & digest
		def create_activation_digest
			self.activation_token  = User.new_token
			self.activation_digest = User.digest(activation_token)
		end

end
