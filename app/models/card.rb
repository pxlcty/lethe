class Card < ActiveRecord::Base
	belongs_to :deck
	has_one :tracker, dependent: :destroy

	accepts_nested_attributes_for :tracker

	mount_uploader :picture, PictureUploader

	default_scope { order('created_at ASC') }

	validate :picture_size

	private
		# Validates the size of an uploaded picture
		def picture_size
			if picture.size > 500.kilobytes
				errors.add(:picture, "should be less than 500kB")
			end
		end

end
