module SessionsHelper

	# Logs in the given user
	# session is a method of Rails. it creates a temp cookie and encrypts its content.
	def log_in(user)
		session[:user_id] = user.id
	end

	# Remember a user in a persistent (long-term-cookie) session.
	def remember(user)
		user.remember # see controller User.rb
		cookies.permanent.signed[:user_id] = user.id # pushes user.id into user_id; encrypts user_id, adds 20 years to it, then bakes a cookie out of it.
		cookies.permanent[:remember_token] = user.remember_token # pushes it for 20 years into a cookie.
	end

	def current_user?(user)
		user == current_user
	end


	# Returns user as per the remember token cookie.
	def current_user
		# check if current session is already active:
		if (user_id = session[:user_id]) # is there an active session with :user_id?
			@current_user ||= User.find_by(id: user_id) # the return @current_user
		# or check if a we should login a persistent cookie :
		elsif (user_id = cookies.signed[:user_id]) # is there a cookie with signed :user_id out there?
			#raise # if this does not break code, it means this code is never executed!
			user = User.find_by(id: user_id)
			if user && user.authenticated?(:remember, cookies[:remember_token]) # cookie user id and authenticated remember_token are true
				log_in user
				@current_user = user
			end
		end
	end

	# Are we logged in?
	def logged_in?
		!current_user.nil?
	end

	# Forgets a persistent session.
	def forget(user)
		user.forget
		cookies.delete(:user_id)
		cookies.delete(:remember_token)
	end

	def log_out
		forget(current_user)
		session.delete(:user_id)
		@current_user = nil
	end

	# Redirects to stored location (or to the default).
	def redirect_back_or(default)
		redirect_to(session[:forwarding_url] || default)
		session.delete(:forwarding_url)
	end

	# Stores the URL trying to be accessed.
	def store_location
		session[:forwarding_url] = request.url if request.get?
	end

end
