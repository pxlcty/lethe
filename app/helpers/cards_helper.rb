module CardsHelper

	def tracker_was_exposed(cards_id)
		@tracker = Tracker.find_by(card_id: cards_id)
		exposureTime = @tracker.exposure_arr.push(Time.zone.now)
		@tracker.update_attribute(:exposure_arr, exposureTime)
	end

	def get_tracker
		return @tracker
	end

	def get_card_from_list(alteration = 0)
		puts ("What the heck does this spit out?")
		puts (cookies.permanent[:cards_list])
		list_arr = cookies.permanent[:cards_list].split(',')
		temp_ref = list_arr.rotate(alteration)
		#cookies.permanent[:cards_list] = (list_arr.class == Array) ? list_arr.join(',') : ''
		return temp_ref[0]
	end

	def get_card_arr_position(alteration = 0)
		cards_list = Card.where(deck_id: @card.deck).ids
		# get current show id:
		current_position = cards_list.index(@card.id)
		wanted_position = current_position + alteration
		if wanted_position == cards_list.length
			wanted_position = 0
		end

		cards_list[wanted_position]
	end

	def create_smart_randomlist
	 	id_level_array = Card.where(deck_id: @card.deck).pluck(:id, :level)
		@learn_list = [] # clear list if existing.
		expose_card_level_multiplication(id_level_array)
		id_level_array.each do |item|
			@learn_list.fill(item[0], @learn_list.size, item[1])
		end
		@learn_list.shuffle! # figure out this? http://stackoverflow.com/questions/19261061/picking-a-random-option-where-each-option-has-a-different-probability-of-being
		@learn_list = no_double_exposures(@learn_list)
		return @learn_list
	end

	def create_static_list
		@learn_list = Card.where(deck_id: @card.deck).ids
	end

	# change exposure levels to your needs.
	def expose_card_level_multiplication(nested_array)
		nested_array.each { |id_level|
				id_level[1] = case id_level[1]
					when 1
						12
					when 2
						8
					when 3
						4
					when 4
						2
					when 5
						1
					end
		}
		return nested_array
	end

	def no_double_exposures(shuffled_arr)
		newArr = []
		# make sure first & last are not the same.
		if(shuffled_arr[0] == shuffled_arr[-1])
			shuffled_arr.pop
		end
		#shuffled_arr.each_cons(2).map{ |a,b| if a == b ? shuffled_arr.delete(b) }
		# shuffled_arr.each_cons(2).map do |a,b|
		# 	if a == b
		# 		puts "#{a} = #{b}"
		# 		shuffled_arr.delete_at(b)
		# 	end
		# end
		#[0,*shuffled_arr].each_cons(2).map {|x,y| y - x }

		previous = nil
	 	for count in shuffled_arr
			#puts "Fresh count is: #{count} & #{previous}"
		 	if previous.nil?
			#	puts "nil was true the first time"
		 		previous = count
				newArr.push(count)
		 		next
			elsif previous != count
			#	puts "ADDING: #{previous} vs #{count}"
				newArr.push(count)
				previous = count
			else
			#	puts "NOT adding: #{previous} vs #{count}"
			end
		#puts "previous changed to #{previous}"
		#puts ""
		end
		return newArr
	end

end
