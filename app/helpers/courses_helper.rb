module CoursesHelper

	def day_count(start_date, end_date)
		return ((end_date - start_date).to_i / 24 / 60 / 60)
	end

	def get_user_name(user_id)
		return User.find(user_id).name
	end

	def get_deck_name(deck_id)
		return Deck.find(deck_id).name
	end

	def get_card_count(deck_id)
		return Card.where(deck_id: deck_id).count
	end

end
