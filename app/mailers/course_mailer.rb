class CourseMailer < ApplicationMailer

	def study_release(deck, user, course)
		puts "Study release mailer here..."
		@deck = deck
		@user = user
		@course = course
		puts "Vars for study mailer passed."
		puts "Waht is user? : #{@user}, #{@user.email}"

		mail to: @user.email, subject: "#{@course.title}: study #{@deck.name} realesed."
	end

	def study_release_teacher_report(study, pupils, deck, course)
		puts "Teacher report mailer here...."
		@study = study
		@pupils = pupils
		@deck = deck
		@course = course
		@teacher = User.find(course.teacher_id)
		mail to: @teacher.email, subject: "Study release report #{@course.title} : #{@deck.name}."
	end

	def student_print(pupil, teacher, deck)
		@pupil = pupil
		@teacher = teacher
		@deck = deck

		mail to: @teacher.email, subject: "#{@pupil.name} printed from the #{@deck.name} study."
		puts "= = "*80
		puts "email should have gone out..."
	end
end
