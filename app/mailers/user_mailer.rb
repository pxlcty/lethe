class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user

    mail to: user.email, subject: "Password reset"
  end

  def teacher_invitation(user, teacher, invite)
    @invite = invite
    @user = user
    @teacher = teacher

    mail to: user.email, subject: "Lethe: Roster Invitation by #{teacher.name}"
  end

  def invitation_accepted(teacher, student)
    @teacher = teacher
    @student = student

    mail to: teacher.email, subject: "#{student.name} accepted roster invitation."
  end
end
