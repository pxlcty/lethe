class ReminderMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.reminder_mailer.study_alert.subject
  #
  def study_alert(deck, last_reminder = false, next_reminder = nil)
    puts "Class::ReminderEmail::study_alert was called with #{deck}"
    @deck = deck
    @user = User.find_by(id: @deck.user_id)
    @last_reminder = last_reminder
    @next_reminder = next_reminder

    mail to: @user.email, subject: "Reminder to practice the #{deck.name} deck."
  end
end
