namespace :pxlcty do
	desc "Copies Study from Course to Pupils, upon release time, and emails all students & teacher."
	task course_study_release: :environment do
						puts "="*80
						non_sent_studies_at_current_time = Study.where("release <= ?", Time.current).where(sent: false)
						puts "This should be the bucket of studies to send: #{non_sent_studies_at_current_time.count}"
						non_sent_studies_at_current_time.each do |study|
							course = Course.find(study.course_id)
							study_cards = Card.where(deck_id: study.deck_id)
							#course_ref = study.course_id
							course_pupils = Pupil.where(course_id: study.course_id)
							#course_pupils.map { |a| puts a.user_id }
							pupils = User.where(id: course_pupils.pluck(:user_id) )
							puts "did we get any pupils? : #{course_pupils.count}"
							#---------- create new deck belonging to User, but has course_id attached ----------
							study_deck = Deck.find(study.deck_id)
							puts "did we get stuyd deck? : #{study_deck.name}, #{study_deck.id}"
							pupils.each do |pupil|
								puts pupil.name
								puts pupil.id
								puts study_cards.count
								pupils_new_deck =  Deck.create(
						  	name: study_deck.name,
						  	description: study_deck.description,
						  	user_id: pupil.id,
							 	tracked: true,
							 	course_id: study.course_id
							  )
								#puts "did we create new deck? : #{pupils_new_deck.id}"
								#---------- create new cards for the deck ----------
								study_cards.each do |study_card|
									study_card = Card.create(
										front: study_card.front,
										back: study_card.back,
										hint: study_card.hint,
										level: 1,
										deck_id: pupils_new_deck.id,
										)
									Tracker.create!(card_id: study_card.id)
								end
								puts "did we create a new card within that deck?"
								CourseMailer.study_release(pupils_new_deck, pupil, course).deliver
								puts "did the pupil get an email?"
							end

							CourseMailer.study_release_teacher_report(study, pupils, study_deck, course).deliver
							study.sent = true
							study.save!
							end

	end
end
