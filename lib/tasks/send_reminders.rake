namespace :pxlcty do
	desc "Check for past-due alerts, send alert, update ActiveRecord for next alert in que."
	task send_reminders: :environment do
		#puts "*** CUSTOM RAKE COMMAND: SEND-REMINDERS IS WORKING!"
		#Find all < Tine.now.
			alert_collection = Reminder.where("next_alert <= ?", Time.current)
			#puts "TOTAL REMINDER RECORDS: #{alert_collection.count}"
			alert_collection.each do |reminder|
				#puts "TO BE SENT AT: #{reminder.next_alert}"
				deck = Deck.find(reminder.deck_id )
				#puts "FROM DECK: #{deck.name}"
				#puts "ALERTS IN CUE LEFT: #{reminder.alerts.count}"

				if reminder.alerts.count == 0
					ReminderMailer.study_alert(deck, true).deliver
					Reminder.find(reminder.id).destroy
				else
					ReminderMailer.study_alert(deck, false, reminder.alerts[0]).deliver
					reminder.next_alert = reminder.alerts.shift
					reminder.save!
					#puts "NEXT ALERT SCHEDULED FOR: #{reminder.next_alert}"
					#puts "ALERTS LEFT IN CUE : #{reminder.alerts.count}"
				end
			end


	end

end
