require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @non_admin = users(:archer)
    @non_shared_user = (:lana)
    @shared_user = (:mallory)
  end

  test "index including pagination" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    User.paginate(page: 1).each do |user|
       assert_select 'a[href=?]', user_path(user), text: user.name
    end
  end

  test "non shared user visible for admin, but not for non-admin" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'a[href=?]', user_path(@non_admin), text: @non_admin.name
    log_in_as(@non_admin)
    assert_not @non_admin.admin
    assert_not @non_admin.shared
    get users_path
    assert_template 'users/index'
    assert user_path(@non_admin)
    assert user_path(@user)
  end

  # test "non shared user not visible" do
  #   log_in_as(@non_admin)
  #   get users_path
  #   assert_template 'users/index'
  #   assert_select 'a[href=?]', user_path(@shared_user), text: @shared_user.name
  #
  # end
end
