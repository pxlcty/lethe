require 'test_helper'

class ReminderMailerTest < ActionMailer::TestCase
  test "study_alert" do
    mail = ReminderMailer.study_alert
    assert_equal "Study alert", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
