# Preview all emails at http://localhost:3000/rails/mailers/reminder_mailer
class ReminderMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/reminder_mailer/study_alert
  def study_alert
    deck = Deck.find(9)
    ReminderMailer.study_alert(deck)
  end

end
